# create class to process inputs
class InputProcessor:
    def wholeNumber(self, userInput):
        # evaluate input for length, numbers and a leading zero
        if len(userInput) == 4 \
        and userInput.isdecimal() \
        and userInput[0] != '0':
            return 'Thanks!'
        else:
            return "That's not a 4-digit whole number."

    def lessThan50(self, userInput):
        # evaluate input for number and do math
        if userInput.isdecimal() \
        and int(userInput) < 50:
            return 'Thanks!'
        # evaluate input for negative number
        elif userInput[0] == '-' \
        and userInput[1:].isdecimal():
            return 'Thanks!'
        else:
            return "That's not an integer less than 50."

    def startWithVowel(self, userInput):
        # evaluate first character of input string
        if userInput[0].lower() in 'aeiou':
            return 'Thanks!'
        else:
            return 'That does not begin with a vowel.'

    def endWithConsonant(self, userInput):
        # evaluate last character of input string
        if userInput[-1].lower() in 'bcdfghjklmnpqrstvwxyz':
            return 'Thanks!'
        else:
            return 'That does not end with a consonant.'



# Input Checker app
# by Michael Macks McCosh (MacksM3)
# version: 3.0 Object-Oriented update
# inspired by Diane Albers
# 25mar2022


# create class to process inputs
class InputProcessor:
    def __init__(self, userInput):
        self.userInput = userInput

    def wholeNumber(self):
        # evaluate input for length, numbers and a leading zero
        if len(self.userInput) == 4 \
                and self.userInput.isdecimal() \
                and self.userInput[0] != '0':
            return 'Thanks!'
        else:
            return "That's not a 4-digit whole number."

    def lessThan50(self):
        # evaluate input for number and do math
        if self.userInput.isdecimal() \
                and int(self.userInput) < 50:
            return 'Thanks!'
        # evaluate input for negative number
        elif self.userInput[0] == '-' \
                and int(self.userInput) < 50:
            return 'Thanks!'
        else:
            return "That's not an integer less than 50."

    def startWithVowel(self):
        # evaluate first character of input string
        if self.userInput[0].lower() in 'aeiou':
            return 'Thanks!'
        else:
            return 'That does not begin with a vowel.'

    def endWithConsonant(self):
        # evaluate last character of input string
        if self.userInput[-1].lower() in 'bcdfghjklmnpqrstvwxyz':
            return 'Thanks!'
        else:
            return 'That does not end with a consonant.'


input_1 = input("Enter a 4-digit whole number: ")
# create object and call method
d1 = InputProcessor(input_1)
print(d1.wholeNumber())

input_2 = input("Enter an integer less than 50: ")
# create object and call method
d2 = InputProcessor(input_2)
print(d2.lessThan50())

input_3 = input("Enter a string that begins with a vowel: ")
# create object and call method
d3 = InputProcessor(input_3)
print(d3.startWithVowel())

input_4 = input("Enter a string that ends with a consonant: ")
# create object and call method
d4 = InputProcessor(input_4)
print(d4.endWithConsonant())




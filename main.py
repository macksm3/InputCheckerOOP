# Input Checker Object-Oriented update v3.0
# by Michael Macks McCosh (MacksM3)
# version: 2.0 inspired by Buda (famcoll)
# version: 3.0 inspired by Diane Albers
# version: 4.0 inspired by MaryEllen2
# 25mar2022

# import module
from InputProcessor import InputProcessor

# create object
my_input = InputProcessor()

# get input and call method
input_1 = input("Enter a 4-digit whole number: ")
print(my_input.wholeNumber(input_1))

input_2 = input("Enter an integer less than 50: ")
print(my_input.lessThan50(input_2))

input_3 = input("Enter a string that begins with a vowel: ")
print(my_input.startWithVowel(input_3))

input_4 = input("Enter a string that ends with a consonant: ")
print(my_input.endWithConsonant(input_4))

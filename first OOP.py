
**********************************************************
# Input Checker app
# by Michael Macks McCosh (MacksM3)
# version: 2.0 Object-Oriented approach
# inspired by Buda (famcoll)
# 24mar2022


# create a function for repeated code
def success_msg():
    print('Thanks!')


# create classes to process inputs
class CheckProcessor:
    def __init__(self, userInput):
        self.userInput = userInput

    def checkUsernum(self):
        # evaluate input for length, numbers and a leading zero
        if len(self.userInput) == 4 and self.userInput.isdecimal(
        ) and self.userInput[0] != '0':
            success_msg()
        else:
            print("That's not a 4-digit whole number.")


class IntProcessor:
    def __init__(self, userInput):
        self.userInput = userInput

    def checkUserint(self):
        # evaluate input for number and do math
        if self.userInput.isdecimal() and int(self.userInput) < 50:
            success_msg()
        # evaluate input for negative number
        elif self.userInput[0] == '-' and int(self.userInput) < 50:
            success_msg()
        else:
            print("That's not an integer less than 50.")


class VowelProcessor:
    def __init__(self, userInput):
        self.userInput = userInput

    def vowelscript(self):
        # evaluate first character of input string
        if self.userInput[0].lower() in 'aeiou':
            success_msg()
        else:
            print('That does not begin with a vowel.')


class ConsonantProcessor:
    def __init__(self, userInput):
        self.userInput = userInput

    def consonantscript(self):
        # evaluate last character of input string
        if self.userInput[-1].lower() in 'bcdfghjklmnpqrstvwxyz':
            success_msg()
        else:
            print('That does not end with a consonant.')


input_1 = input("Enter a 4-digit whole number: ")
d1 = CheckProcessor(input_1)
d1.checkUsernum()

input_2 = input("Enter an integer less than 50: ")
d2 = IntProcessor(input_2)
d2.checkUserint()

input_3 = input("Enter a string that begins with a vowel: ")
d3 = VowelProcessor(input_3)
d3.vowelscript()

input_4 = input("Enter a string that ends with a consonant: ")
d4 = ConsonantProcessor(input_4)
d4.consonantscript()


100²
🏀
200¾

©

